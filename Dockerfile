FROM registry.gitlab.com/johnjawed/runfaster/master:43bd2d87d74eb262cd460a80f0df993dcf1d85e6
ADD ./ /app/
WORKDIR /app
ENV PORT 5000
EXPOSE 5000

CMD ["sh", "-c", "while :; do /bin/herokuish procfile start web; done"]